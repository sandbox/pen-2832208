<?php

namespace Drupal\meta_custom\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\meta_custom\Controller\MetaCustomUpdate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MetaCustomForm.
 *
 * @package Drupal\meta_custom\Form
 */
class MetaCustomForm extends FormBase {

  protected $database;

  protected $currentUser;

  protected $route;

  /**
   * {@inheritdoc}
   *
   * @param Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database, $currentUser, $route) {
    $this->database = $database;
    $this->currenUser = $currentUser;
    $this->route = $route;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('current_user'), $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'meta_custom_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $link = NULL;
    $title = NULL;
    $description = NULL;
    if (!empty($this->getRequest()->query)) {
      $mid = $this->getRequest()->query->get('mid');
      if (isset($mid)) {
        $result = MetaCustomUpdate::metaCheck($this->database, $mid, TRUE);
        if (is_object($result)) {
          $link = $result->link;
          $title = $result->title;
          $description = $result->description;
        }
        $form['mid'] = [
          '#type' => 'hidden',
          '#default_value' => $mid,
        ];
        $form['delete'] = [
          '#type' => 'link',
          '#title' => $this->t('Delete'),
          '#url' => Url::fromRoute('meta_custom.meta_custom_delete_form', [
              'mid' => $mid,
            ]
          ),
          '#weight' => 10,
        ];
      }
    }
    $form['link'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Link'),
      '#description' => $this->t('Enter the route.'),
      '#required' => TRUE,
      '#default_value' => $link,
    ];
    $form['title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Meta Title'),
      '#required' => TRUE,
      '#default_value' => $title,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Meta Description'),
      '#description' => $this->t('Meta Description'),
      '#required' => TRUE,
      '#default_value' => $description,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $links = [];
    $link = $form_state->getValue('link');
    $links[] = $link;
    $result = $this->route->getRoutesByNames($links);
    if (count($result) < 1) {
      $form_state->setErrorByName('link', 'The Route you have entered doesn\'t exist.');
    }
    $mid = $form_state->getValue('mid');
    if (!isset($mid)) {
      $check = MetaCustomUpdate::metaLinkCheck($this->database, $link);
      if ($check === TRUE) {
        $form_state->setErrorByName('link', 'Meta Tags for this route already exists.');
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $title = $form_state->getValue('title');
    $link = $form_state->getValue('link');
    $description = $form_state->getValue('description');
    $mid = $form_state->getValue('mid');
    if (isset($mid)) {
      $arr = [
        'uid' => $this->currenUser->id(),
        'link' => $link,
        'title' => $title,
        'description' => $description,
        'updated' => REQUEST_TIME,
      ];
      $update = MetaCustomUpdate::metaUpdateTable($this->database, $arr, $mid);
      if ($update === TRUE) {
        drupal_set_message($this->t('Data Updated Successfully'));
      }
    }
    else {
      $arr = [
        'uid' => $this->currenUser->id(),
        'link' => $link,
        'title' => $title,
        'description' => $description,
        'created' => REQUEST_TIME,
        'updated' => REQUEST_TIME,
      ];
      $insert = MetaCustomUpdate::metaInsert($this->database, $arr);
      if ($insert === TRUE) {
        drupal_set_message($this->t('Data Inserted Successfully'));
      }
    }
  }

}
