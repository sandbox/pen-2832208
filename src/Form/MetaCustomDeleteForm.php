<?php

namespace Drupal\meta_custom\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\meta_custom\Controller\MetaCustomUpdate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MetaCustomForm.
 *
 * @package Drupal\meta_custom\Form
 */
class MetaCustomDeleteForm extends ConfirmFormBase {

  protected $database;

  /**
   * {@inheritdoc}
   *
   * @param Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('current_user'), $container->get('redirect_response_subscriber')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'meta_custom_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Do you want to delete %id?', ['%id' => $this->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('meta_custom.meta_custom_update_metaupdate');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('This action can not be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete it!');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return t('Cancel');
  }

  /**
   * {@inheritdoc}
   *
   * @param int $mid
   *   (optional) The ID of the item to be deleted.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mid = NULL) {
    $this->id = $mid;
    $result = MetaCustomUpdate::metaCheck($this->database, $mid);
    if ($result === FALSE) {
      return $this->redirect('meta_custom.meta_custom_update_metaupdate');
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $delete = MetaCustomUpdate::metaDelete($this->database, $this->id);
    if ($delete === TRUE) {
      drupal_set_message(t('Data Deleted Successfully'));
      $url = Url::fromRoute('meta_custom.meta_custom_update_metaupdate');
      $form_state->setRedirectUrl($url);
    }
  }

}
