<?php

namespace Drupal\meta_custom\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MetaCustomUpdate.
 *
 * @package Drupal\meta_custom\Controller
 */
class MetaCustomUpdate extends ControllerBase {

  protected $database;

  /**
   * {@inheritdoc}
   *
   * @param Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Metaupdate.
   *
   * @return string
   *   Return Hello string.
   */
  public function metaupdate() {
    $data = $this->database->select('meta_custom', 'm');
    $data->fields('m', [
        'uid',
        'link',
        'title',
        'description',
        'created',
        'updated',
        'mid',
      ]
    );
    $header = [
      'ID',
      'Link',
      'Title',
      'Description',
      'Created',
      'Updated',
      'Edit',
    ];
    $table_sort = $data->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(10);
    $result = $pager->execute();
    $result->allowRowCount = TRUE;
    if ($result->rowCount() > 0) {
      foreach ($result as $row) {
        $row->created = date('d-m-Y H:i:s', $row->created);
        $row->updated = date('d-m-Y H:i:s', $row->updated);
        // Internal path (defined by a route in Drupal 8).
        $internal_link = Link::createFromRoute('edit', 'meta_custom.meta_custom_form', [
            'mid' => $row->mid,
          ]
        );
        $row = (array) $row;
        $row['mid'] = $internal_link;
        $rows[] = ['data' => (array) $row, 'style' => 'word-break:break-all;'];
      }
    }
    $build = [
      '#markup' => 'List of All Custom Tags',
    ];

    $build['location_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => 'No items available',
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Metadelete.
   *
   * @return bool
   *   Return TRUE if data is deleted or FALSE otherwise.
   */
  public function metaDelete($obj, $mid) {
    return $obj->delete('meta_custom')
      ->condition('mid', $mid, '=')
      ->execute() === NULL ? FALSE : TRUE;
  }

  /**
   * Metacheck.
   *
   * @return string
   *   Return check.
   */
  public function metaCheck($obj, $mid, $sendData = FALSE) {
    // Return TRUE if $sendData is FALSE
    // Else Return $value object.
    $data = $obj->select('meta_custom', 'm')
      ->fields('m', ['link', 'title', 'description'])
      ->condition('m.mid', $mid, '=')
      ->execute();
    $data->allowRowCount = TRUE;
    if ($data->rowCount() > 0 && $sendData === FALSE) {
      return TRUE;
    }
    else {
      foreach ($data as $value) {
        return $value;
      }
    }
    return FALSE;
  }

  /**
   * Metalinkcheck.
   *
   * @return string
   *   Return TRUE if count is greater then 0 or FALSE if 0.
   */
  public function metaLinkCheck($obj, $link) {
    $link_exist = $obj->select('meta_custom', 'm')
      ->fields('m', ['link'])
      ->condition('m.link', $link, '=')
      ->execute();
    $link_exist->allowRowCount = TRUE;
    return ($link_exist->rowCount() > 0) ? TRUE : FALSE;
  }

  /**
   * Metalinkcheck.
   *
   * @return bool
   *   Return TRUE or FALSE on data operations
   */
  public function metaUpdateTable($obj, $fields, $mid) {
    return $obj->update('meta_custom')
      ->fields($fields)
      ->condition('mid', $mid, '=')
      ->execute() === NULL ? FALSE : TRUE;
  }

  /**
   * Metalinkcheck.
   *
   * @return bool
   *   Return TRUE or FALSE on data operations.
   */
  public function metaInsert($obj, $fields) {
    return $obj->insert('meta_custom')
      ->fields($fields)
      ->execute() === NULL ? FALSE : TRUE;
  }

}
