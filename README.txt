Meta Custom
===================

Meta Custom module help users to set Meta tags for custom absolute urls.
Meta Title, Meta Description can be added based on the link.

Configuration:
--------------
Meta Custom module have a form to add meta tags for custom urls at:
admin/config/system/meta_custom.

List of added content may be found at:
admin/config/system/custom-meta-update
